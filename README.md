# go-rosalind

go-rosalind is a command-line utility for running solutions to [Rosalind.info](rosalind.info) problems that were written in go.

This is mostly just used by me to consolidate some repeated code and practice writing stuff in Go.
