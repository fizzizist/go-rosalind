package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/fizzizist/go-rosalind/pkg/solutions"
)

var consCmd = &cobra.Command{
	Use:   "cons",
	Short: "Finds a Motif in input DNA",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		solutions.Cons(args[0])
	},
}

func init() {
	rootCmd.AddCommand(consCmd)
}
