package cmd

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Use:   "go-rosalind",
	Short: "An app for running Rosalind solutions",
}

func Execute() error {
	return rootCmd.Execute()
}
