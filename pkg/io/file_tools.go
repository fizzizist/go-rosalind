package io

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func FileToStringArray(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var fileLines []string

	for scanner.Scan() {
		fileLines = append(fileLines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	file.Close()

	return fileLines, nil
}

func WriteStringsToFile(content []string, filepath string) error {
	file, err := os.Create(filepath)
	if err != nil {
		return err
	}

	w := bufio.NewWriter(file)

	for _, line := range content {
		_, err = fmt.Fprintln(w, line)
		if err != nil {
			return err
		}
	}

	if err = w.Flush(); err != nil {
		return err
	}
	if err = file.Close(); err != nil {
		return err
	}

	return nil
}

// ParseFasta parses a .fasta formatted file returning a list of labels and a list of
// content in separate string arrays.
func ParseFasta(filename string) ([]string, []string, error) {
	lines, err := FileToStringArray(filename)
	if err != nil {
		return nil, nil, err
	}

	var labels []string
	var content []string
	var currString []string
	for _, line := range lines {
		if line[0] == '>' {
			labels = append(labels, line)
			if len(currString) > 0 {
				content = append(content, strings.Join(currString, ""))
				currString = []string{}
			}
			continue
		}
		currString = append(currString, line)
	}
	if len(currString) > 0 {
		content = append(content, strings.Join(currString, ""))
	}

	return labels, content, nil
}
