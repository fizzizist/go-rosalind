package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fizzizist/go-rosalind/pkg/io"
)

func FindMotifs(filename string) {
	lines, err := io.FileToStringArray(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}
	if len(lines) != 2 {
		log.Fatalf("Input file should have exactly 2 lines, a DNA string and a motif")
	}

	dna := lines[0]
	motif := lines[1]
	motLen := len(motif)
	var positions []string

	for i := 0; i <= len(dna)-motLen; i++ {
		if motif == dna[i:i+motLen] {
			positions = append(positions, strconv.Itoa(i+1))
		}
	}
	fmt.Println(strings.Join(positions, " "))

}
