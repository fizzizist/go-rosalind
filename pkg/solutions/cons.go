package solutions

import (
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/fizzizist/go-rosalind/pkg/io"
)

func CompileOutput(consensus string, profile [][]int) []string {
	output := []string{consensus}
	idxMap := []string{"A", "C", "G", "T"}

	for i, val := range profile {
		var strProf []string
		for _, inVal := range val {
			strProf = append(strProf, strconv.Itoa(inVal))
		}
		output = append(output, fmt.Sprintf("%s: %s", idxMap[i], strings.Join(strProf, " ")))
	}

	return output
}

func Cons(filename string) {
	_, lines, err := io.ParseFasta(filename)
	if err != nil {
		log.Fatalf("Failed to read file: %s", err)
	}

	profile := make([][]int, 4) // 1 for each codon
	for i := range profile {
		profile[i] = make([]int, len(lines[1]))
	}

	for _, value := range lines {
		for j, s := range value {
			switch {
			case s == 'A':
				profile[0][j]++
			case s == 'C':
				profile[1][j]++
			case s == 'G':
				profile[2][j]++
			case s == 'T':
				profile[3][j]++
			}
		}
	}

	var consensus []string
	idxMap := []string{"A", "C", "G", "T"}
	for i := range profile[0] {
		maxCons := "A"
		maxVal := 0
		for j, s := range idxMap {
			if profile[j][i] > maxVal {
				maxCons = s
				maxVal = profile[j][i]
			}
		}
		consensus = append(consensus, maxCons)
	}

	fmt.Println("Writing output file...")
	err = io.WriteStringsToFile(
		CompileOutput(strings.Join(consensus, ""), profile),
		"results/cons.txt",
	)
	if err != nil {
		log.Fatalf("Failed writing output file: %s", err)
	}
	fmt.Println("Done.")
}
